Feature: Login
  As a user
  I want to be able to login
  So that I can access my account

  Background: I am logged out

  Scenario: Successful login
    Given I am on the "/login" page
    When I enter my username and password
    And I click the "Login" button
    Then I should see "Go to My Tasks"

  Scenario: Invalid login
    Given I am on the "/login" page
    When I enter an invalid username or password
    And I click the "Login" button
    Then I should see "Please enter a correct username and password."

  Scenario: Successful login after registration
    Given I am a registered user
    And I am on the "/login" page
    When I enter a registered username and password
    And I click the "Login" button
    Then I should see "Go to My Tasks"
