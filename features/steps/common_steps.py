from behave import given, when, then
from selenium.webdriver.common.by import By


@given('I am on the "{page}" page')
def step_impl(context, page):
    context.browser.get(context.base_url + page)


@when('I enter "{text}" in the "{field}" field')
def step_impl(context, text, field):
    context.browser.find_element(By.NAME, field).send_keys(text)


@when('I click the "{button}" button')
def step_impl(context, button):
    context.browser.find_element(By.XPATH, f"//button[text()='{button}']").click()


@then('I should see "{text}"')
def step_impl(context, text):
    assert text in context.browser.page_source
