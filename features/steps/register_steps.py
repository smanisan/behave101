from behave import given, when, then
from features.utils import *


@when('I enter valid registration details')
def step_impl(context):
    user = {'username': generate_random_str(), 'firstname': generate_random_str(), 'lastname': generate_random_str(),
            'email': generate_random_email(), 'password': generate_random_str("&^")}
    context.user = user
    context.execute_steps(
        f'''
        when I enter "{user['username']}" in the "username" field
        and I enter "{user['firstname']}" in the "first_name" field
        and I enter "{user['lastname']}" in the "last_name" field
        and I enter "{user['email']}" in the "email" field
        and I enter "{user['password']}" in the "password1" field
        and I enter "{user['password']}" in the "password2" field
        '''
    )


@given('I am a registered user')
def step_impl(context):
    context.execute_steps(
        f'''
        Given I am on the "/register" page
        When I enter valid registration details
        And I click the "Sign Up" button
        
        '''
    )
