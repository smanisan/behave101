Feature: User Registration
  As a user
  I want to be able to register for an account
  So that I can access the application's features

  Background: I am logged out

  Scenario: Successful registration with valid credentials
    Given I am on the "/register" page
    When I enter valid registration details
    And I click the "Sign Up" button
    Then I should see "You can login now"
