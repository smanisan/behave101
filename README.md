# A demo project for PyCon PH 2024

## Installation
- Create a virtualenv: `$ virtualenv venv`
- Activate: `$ source venv/bin/activate`
- Install requirements: `$ pip install -r requirements.txt`

## Run the test

- To run the whole test: `$ behave`
- To run a feature file: `$ behave <path_to_feature_file>`

